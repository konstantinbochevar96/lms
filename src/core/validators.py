import re

from django.core.exceptions import ValidationError


BLACKLIST = ['zzz.com', 'mail.ru', 'yandex.ru']


def validate_email_for_prohibited_domain(value):

    _, _, email_domain = value.partition('@')

    pattern = r'@(\w+\.)+\w{1,3}[^\d]\b'

    if email_domain in BLACKLIST:
        raise ValidationError('Prohibited domain')

    if not re.findall(pattern, value):
        raise ValidationError('Email is incorrect')

    return value


def validate_phone_number(value):

    pattern = r'(\(\d{3}\)|\+\d{2}\(\d{3}\))\d{3}\-\d{4}\b'

    if not re.match(pattern, value):
        raise ValidationError('Phone number is incorrect')

    return value
