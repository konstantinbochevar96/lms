from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models # noqa

# Create your models here.
from core.validators import validate_email_for_prohibited_domain, validate_phone_number


class Person(models.Model):
    class Meta:
        abstract = True

    first_name = models.CharField(max_length=64, null=False)
    last_name = models.CharField(max_length=84, null=False)
    age = models.IntegerField(null=False, default=33, validators=[
        MinValueValidator(20),
        MaxValueValidator(85),
    ])

    email = models.EmailField(max_length=64, validators=[
        validate_email_for_prohibited_domain
    ])

    phone_number = models.CharField(null=True, max_length=16, validators=[
        validate_phone_number
    ])

    def __str__(self):
        return f'{self.first_name}, {self.last_name}, {self.age},\
        {self.email}, {self.phone_number}'

    def full_name(self):
        return f'{self.first_name} {self.last_name}'


# class Logger(models.Model):
#     user = models.ForeignKey(to=User, on_delete=models.CASCADE)
#     path = models.CharField(max_length=128)
#     create_date = models.DateTimeField(auto_now_add=True)
#     execution_time = models.FloatField()
#     query_params = models.CharField(max_length=64, null=True)
#     info = models.CharField(max_length=128, null=True)
