import time

from core_test.models import Logger


class PerfTrackerMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        start_time = time.time()

        response = self.get_response(request)

        execution_time = time.time() - start_time

        if request.user.is_authenticated and callable(request.user):
            obj = Logger(
                user=request.user,
                path=request.path,
                execution_time=execution_time,
            )
            obj.save()

        return response
