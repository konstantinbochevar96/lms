import random

from django.db import models

# Create your models here.
from faker import Faker

from core.models import Person

from groups.models import Group


class Teacher(Person):

    birth_date = models.DateField(null=True)

    group = models.ForeignKey(
        to=Group,
        null=True,
        on_delete=models.SET_NULL,
        related_name='teachers'
    )

    @classmethod
    def generate_teachers(cls, count):
        faker = Faker()

        for _ in range(count):
            object_1 = cls(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                age=random.randint(20, 85),
                email=faker.email(),
            )
            object_1.save()
