import django_filters

from django.core.exceptions import ValidationError
from django.forms import ModelForm

from teachers.models import Teacher

SHORT_LENGTH = 13


class TeacherFilter(django_filters.FilterSet):
    class Meta:
        model = Teacher
        fields = {
            'age': ['gte', 'lte'],
            'first_name': ['exact', 'startswith', 'icontains'],
            'last_name': ['exact', 'startswith', 'icontains'],
        }


class TeacherBaseForm(ModelForm):

    class Meta:
        model = Teacher
        fields = '__all__'

    def clean_phone_number(self):

        phone_number = self.cleaned_data['phone_number']

        if len(phone_number) == SHORT_LENGTH:
            phone_number = '+38' + phone_number

        return phone_number

    def clean_email(self):

        email = self.cleaned_data['email']

        teachers = Teacher.objects.filter(email=email).exclude(id=self.instance.id)

        if teachers:
            raise ValidationError('Email already registered')

        return email


class TeacherCreateForm(TeacherBaseForm):
    pass


class TeacherUpdateForm(TeacherBaseForm):
    class Meta(TeacherBaseForm.Meta):
        exclude = ['age']
