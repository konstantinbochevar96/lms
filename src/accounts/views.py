from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from django.http import HttpResponseRedirect
from django.shortcuts import render # noqa
from django.urls import reverse_lazy, reverse
from django.views.generic import CreateView
from django.views.generic.edit import ProcessFormView, DeleteView

from accounts.forms import AccountRegistrationForm, AccountUpdateForm, AccountProfileUpdateForm


# Create your views here.


class AccountRegistrationView(CreateView):
    model = User
    template_name = 'registration.html'
    success_url = reverse_lazy('accounts:login')
    form_class = AccountRegistrationForm

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, 'User successfully registered')
        return result


class AccountLoginView(LoginView):
    success_url = reverse_lazy('index')
    template_name = 'login.html'

    def get_redirect_url(self):
        if self.request.GET.get('next'):
            return self.request.GET.get('next')
        return reverse('index')

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, f'User {self.request.user} successfully logged in')
        return result


class AccountLogoutView(LogoutView):
    template_name = 'logout.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            messages.info(self.request, f'User {self.request.user} has been logged out')
        return super().dispatch(request, *args, **kwargs)


class AccountUpdateView(ProcessFormView):
    # model = User
    # template_name = 'profile.html'
    # success_url = reverse_lazy('index')
    # form_class = AccountUpdateForm
    #
    # def get_object(self, queryset=None):
    #     return self.request.user
    #
    # def form_valid(self, form):
    #     result = super().form_valid(form)
    #     messages.success(self.request, 'Profile successfully updated')
    #     return result

    def get(self, request, *args, **kwargs):
        user = self.request.user
        profile = self.request.user.profile

        user_form = AccountUpdateForm(instance=user)
        profile_form = AccountProfileUpdateForm(instance=profile)

        return render(
            request=request,
            template_name='profile.html',
            context={
                'user_form': user_form,
                'profile_form': profile_form,
            }
        )

    def post(self, request, *args, **kwargs):
        user = self.request.user
        profile = self.request.user.profile

        user_form = AccountUpdateForm(
            data=request.POST,
            instance=user
        )

        profile_form = AccountProfileUpdateForm(
            data=request.POST,
            files=request.FILES,
            instance=profile
        )

        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(self.request, 'Profile successfully updated')
            return HttpResponseRedirect(reverse('accounts:profile'))

        return render(
            request=request,
            template_name='profile.html',
            context={
                'user_form': user_form,
                'profile_form': profile_form
            }
        )


class AccountDeleteView(DeleteView):
    model = User
    template_name = 'delete.html'
    success_url = reverse_lazy('index')

    def get_object(self, queryset=None):
        return self.request.user


class UserPasswordChangeView(PasswordChangeView):
    success_url = reverse_lazy('accounts:profile')
    template_name = 'password-change.html'
