from PIL import Image

from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.forms import ModelForm

from accounts.models import Profile


class AccountRegistrationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        fields = ['username', 'first_name', 'last_name', 'email']


class AccountUpdateForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        fields = ['username', 'first_name', 'last_name', 'email']


class AccountProfileUpdateForm(ModelForm):
    class Meta:
        model = Profile
        fields = ['image', 'interests']

    def save(self, commit=True):
        super().save(commit)
        image = Image.open(self.instance.image)
        image.thumbnail((300, 300), Image.ANTIALIAS)
        image.save(self.instance.image.path)
