from django.contrib.auth.models import User
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from accounts.models import Profile


@receiver(post_save, sender=User)
def save_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_delete, sender=User)
def delete_profile(sender, instance, **kwargs):
    if instance.profile.image.name != "default.jpg":
        instance.profile.image.delete()
