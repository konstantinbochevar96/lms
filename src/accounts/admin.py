from django.contrib import admin # noqa

# Register your models here.
from accounts.models import Profile

admin.site.register(Profile)
