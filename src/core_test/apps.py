from django.apps import AppConfig


class CoreTestConfig(AppConfig):
    name = 'core_test'
