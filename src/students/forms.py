import django_filters

from django.core.exceptions import ValidationError
from django.forms import ModelForm

from students.models import Student

SHORT_LENGTH = 13


class StudentFilter(django_filters.FilterSet):
    class Meta:
        model = Student
        fields = {
            'age': ['gte', 'lte'],
            'first_name': ['exact', 'startswith', 'icontains'],
            'last_name': ['exact', 'startswith', 'icontains'],
        }


class StudentBaseForm(ModelForm):

    class Meta:
        model = Student
        fields = '__all__'

    def clean_phone_number(self):

        phone_number = self.cleaned_data['phone_number']

        if len(phone_number) == SHORT_LENGTH:
            phone_number = '+38' + phone_number

        return phone_number

    def clean_email(self):

        email = self.cleaned_data['email']

        students = Student.objects.filter(email=email).exclude(id=self.instance.id)

        if students:
            raise ValidationError('Email already registered')

        return email

    def clean(self):
        result = super().clean()
        enroll_date = self.cleaned_data['enroll_date']
        graduate_date = self.cleaned_data['graduate_date']

        if enroll_date > graduate_date:
            raise ValidationError("Enroll date couldn't be after graduate date")

        return result


class StudentCreateForm(StudentBaseForm):
    pass


class StudentUpdateForm(StudentBaseForm):
    class Meta(StudentBaseForm.Meta):
        exclude = ['age']
