import datetime
import random

from django.db import models

# Create your models here.
from faker import Faker

from core.models import Person

from groups.models import Group


class Student(Person):

    enroll_date = models.DateField(default=datetime.date.today)
    graduate_date = models.DateField(default=datetime.date.today())

    group = models.ForeignKey(
        to=Group,
        null=True,
        on_delete=models.SET_NULL,
        related_name='students'
    )

    @classmethod
    def generate_students(cls, count):
        faker = Faker()
        groups = list(Group.objects.all())

        for _ in range(count):
            object_ = cls(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                age=random.randint(20, 85),
                email=faker.email(),
                group=random.choice(groups)
            )
            object_.save()
