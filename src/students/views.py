from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import UpdateView, DeleteView, CreateView, ListView

# Create your views here.
from students.forms import StudentCreateForm, StudentUpdateForm, StudentFilter
from students.models import Student


class StudentListView(LoginRequiredMixin, ListView):
    model = Student
    template_name = 'students-list.html'
    login_url = reverse_lazy('accounts:login')
    paginate_by = 5
    context_object_name = 'students'

    def get_filter(self):
        return StudentFilter(
            data=self.request.GET,
            queryset=self.model.objects.all()
        )

    def get_queryset(self):
        return self.get_filter().qs.select_related('group', 'headed_group').order_by('-id')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = self.get_filter()
        return context


class StudentUpdateView(UpdateView):
    model = Student
    form_class = StudentUpdateForm
    success_url = reverse_lazy('students:list')
    template_name = 'students-update.html'
    pk_url_kwarg = 'id'


class StudentCreateView(CreateView):
    model = Student
    form_class = StudentCreateForm
    success_url = reverse_lazy('students:list')
    template_name = 'students-create.html'
    pk_url_kwarg = "id"


class StudentDeleteView(DeleteView):
    model = Student
    success_url = reverse_lazy('students:list')
    template_name = 'students-delete.html'
    pk_url_kwarg = "id"
