# Generated by Django 3.1.4 on 2021-01-13 23:32

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('groups', '0002_auto_20201225_0103'),
        ('students', '0007_auto_20210106_2241'),
    ]

    operations = [
        migrations.AddField(
            model_name='student',
            name='group',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='students', to='groups.group'),
        ),
        migrations.AlterField(
            model_name='student',
            name='graduate_date',
            field=models.DateField(default=datetime.date(2021, 1, 13)),
        ),
    ]
