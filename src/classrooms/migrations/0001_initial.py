# Generated by Django 3.1.4 on 2021-01-20 23:56

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('groups', '0003_group_headman'),
    ]

    operations = [
        migrations.CreateModel(
            name='Classroom',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.IntegerField()),
                ('group', models.ManyToManyField(related_name='classrooms', to='groups.Group')),
            ],
        ),
    ]
