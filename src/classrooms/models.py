from django.db import models

# Create your models here.


class Classroom(models.Model):
    number = models.IntegerField(null=False)

    group = models.ManyToManyField(
        to='groups.Group',
        related_name='classrooms'
    )

    def __str__(self):
        return f'{self.number}'
