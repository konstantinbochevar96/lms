import itertools


class frange:  # noqa
    def __init__(self, start, stop=None, step=1):
        self.start = start
        self.stop = stop
        self.step = step

        if stop is None:
            self.stop = start
            self.start = 0

    def __next__(self):
        if (self.step > 0 and self.start < self.stop) or (self.step < 0 and self.start > self.stop):
            result = self.start
            self.start += self.step
            return result
        else:
            raise StopIteration

    def __iter__(self):
        return self


'''Tests for frange()'''


assert(list(frange(5)) == [0, 1, 2, 3, 4])
assert(list(frange(2, 5)) == [2, 3, 4])
assert(list(frange(2, 10, 2)) == [2, 4, 6, 8])
assert(list(frange(10, 2, -2)) == [10, 8, 6, 4])
assert(list(frange(2, 5.5, 1.5)) == [2, 3.5, 5])
assert(list(frange(1, 5)) == [1, 2, 3, 4])
assert(list(frange(0, 5)) == [0, 1, 2, 3, 4])
assert(list(frange(0, 0)) == [])
assert(list(frange(100, 0)) == [])
assert(list(itertools.islice(frange(0, float(10**10), 1.0), 0, 4)) == [0, 1.0, 2.0, 3.0])

print('frange SUCCESS!')



class imap:  # noqa
    def __init__(self, func, *args):
        self.func = func
        self.args_list = [i for i in zip(*args)]

    def __next__(self):
        if self.args_list:
            return self.func(*self.args_list.pop(0))
        else:
            raise StopIteration

    def __iter__(self):
        return self


'''Tests for imap()'''


numbers = [2, 4, 6, 8, 10]
powers = [1, 2, 3, 4, 5]


assert(list(imap(pow, numbers, powers)) == [2, 16, 216, 4096, 100000])
assert(list(imap(lambda x: x * 3 + 3, [2, 3, 4, 5, 6])) == [9, 12, 15, 18, 21])
assert(list(imap(ord, 'Sample text')) == [83, 97, 109, 112, 108, 101, 32, 116, 101, 120, 116])
assert(list(imap(abs, (-2.2, 15, -26, 3.3, 0))) == [2.2, 15, 26, 3.3, 0])

print('imap SUCCESS!')
