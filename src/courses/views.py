from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render # noqa
from django.urls import reverse_lazy
from django.views.generic import ListView, DeleteView, CreateView, UpdateView
# Create your views here.

from courses.forms import CourseFilter, CourseUpdateForm, CourseCreateForm
from courses.models import Course


class CourseListView(LoginRequiredMixin, ListView):
    model = Course
    template_name = 'courses-list.html'
    login_url = reverse_lazy('accounts:login')

    def get_filter(self):
        return CourseFilter(
            data=self.request.GET,
            queryset=self.model.objects.all().select_related('group').prefetch_related('teachers')
        )

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = self.get_filter()
        return context


class CourseDeleteView(DeleteView):
    model = Course
    success_url = reverse_lazy('courses:list')
    template_name = 'courses-delete.html'
    pk_url_kwarg = "id"


class CourseCreateView(CreateView):
    model = Course
    form_class = CourseCreateForm
    success_url = reverse_lazy('courses:list')
    template_name = 'courses-create.html'
    pk_url_kwarg = "id"


class CourseUpdateView(UpdateView):
    model = Course
    form_class = CourseUpdateForm
    success_url = reverse_lazy('courses:list')
    template_name = 'courses-update.html'
    pk_url_kwarg = 'id'
