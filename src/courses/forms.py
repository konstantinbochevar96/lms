import django_filters

from django.forms import ModelForm

from courses.models import Course


class CourseFilter(django_filters.FilterSet):
    class Meta:
        model = Course
        fields = {
            'name': ['exact', 'icontains']
        }


class CourseBaseForm(ModelForm):
    class Meta:
        model = Course
        fields = '__all__'


class CourseCreateForm(CourseBaseForm):
    pass


class CourseUpdateForm(CourseBaseForm):
    pass
