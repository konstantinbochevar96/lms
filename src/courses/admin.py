from django.contrib import admin # noqa

# Register your models here.
from courses.models import Course


class CourseAdmin(admin.ModelAdmin):
    list_per_page = 10
    list_display = ['name', 'difficulty', 'group']
    list_select_related = ['group']


admin.site.register(Course, CourseAdmin)
