import random

from django.db import models

# Create your models here.


class Course(models.Model):
    class DIFFICULTY(models.IntegerChoices):
        TRIVIAL = 0, "Trivial"
        EASY = 1, "Easy"
        MEDIUM = 2, "Medium"
        EXPERT = 3, "Expert"
        HARDCORE = 4, "Hardcore"

    name = models.CharField(max_length=64, null=False)

    group = models.OneToOneField(
        to='groups.Group',
        on_delete=models.SET_NULL,
        null=True,
        related_name='group_course'
    )

    teachers = models.ManyToManyField(
        to='teachers.Teacher',
        related_name='courses'
    )

    difficulty = models.PositiveSmallIntegerField(
        choices=DIFFICULTY.choices,
        default=DIFFICULTY.MEDIUM
    )

    def __str__(self):
        return f'{self.name}, {self.difficulty}'

    def get_teachers(self):
        return ', '.join(
            teacher.full_name()
            for teacher in self.teachers.all()
        )

    @classmethod
    def generate_course(cls, count):
        courses = ['JavaScript', 'Python', 'PHP', 'C++', 'Java']

        for _ in range(count):
            obj = cls(
                name=random.choice(courses),
                difficulty=random.choice(cls.DIFFICULTY.values)
            )
            obj.save()
