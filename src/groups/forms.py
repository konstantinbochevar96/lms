import django_filters

from django.forms import ModelForm, ChoiceField

from groups.models import Group

from students.models import Student


class GroupFilter(django_filters.FilterSet):
    class Meta:
        model = Group
        fields = {
            'course': ['gte', 'lte'],
            'name': ['exact', 'startswith', 'icontains'],
            'faculty': ['exact', 'startswith', 'icontains'],
        }


class GroupBaseForm(ModelForm):
    class Meta:
        model = Group
        fields = '__all__'


class GroupCreateForm(GroupBaseForm):
    pass


class GroupUpdateForm(GroupBaseForm):
    class Meta(GroupBaseForm.Meta):
        exclude = ['headman']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        students = [(st.id, st.full_name()) for st in self.instance.students.all()]
        self.fields['headman_field'] = ChoiceField(
            choices=students,
            initial=self.instance.headman.id
        )

    def save(self, commit=True):
        headman = Student.objects.get(id=self.cleaned_data['headman_field'])
        self.instance.headman = headman
        return super().save(commit)
