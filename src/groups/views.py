from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import UpdateView, DeleteView, CreateView, ListView

# Create your views here.

from groups.forms import GroupCreateForm, GroupUpdateForm, GroupFilter
from groups.models import Group


class GroupListView(LoginRequiredMixin, ListView):
    model = Group
    template_name = 'groups-list.html'
    login_url = reverse_lazy('accounts:login')
    paginate_by = 1
    context_object_name = 'groups'

    def get_filter(self):
        return GroupFilter(
            data=self.request.GET,
            queryset=self.model.objects.all().select_related('headman')
        )

    def get_queryset(self):
        return self.get_filter().qs

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = self.get_filter()
        return context


class GroupDeleteView(DeleteView):
    model = Group
    success_url = reverse_lazy('groups:list')
    template_name = 'groups-delete.html'
    pk_url_kwarg = "id"


class GroupCreateView(CreateView):
    model = Group
    form_class = GroupCreateForm
    success_url = reverse_lazy('groups:list')
    template_name = 'groups-create.html'
    pk_url_kwarg = "id"


class GroupUpdateView(UpdateView):

    model = Group
    form_class = GroupUpdateForm
    success_url = reverse_lazy('groups:list')
    template_name = 'groups-update.html'
    pk_url_kwarg = 'id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['students'] = self.get_object().students.select_related('group', 'headed_group').all()
        context['teachers'] = self.get_object().teachers.select_related('group').all()
        return context
